﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using OnlineShop.Data.Models;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services;

namespace OnlineShop.DomainTests
{
    public class CustomerServiceTests
    {
        private CustomerRepository _customerRepository;
        private ProductRepository _productRepository;
        private List<Product> _products; 
        
        private CustomerService _service;

        [SetUp]
        public void SetUp()
        {
            _customerRepository = new CustomerRepository();
            _productRepository = new ProductRepository();

            _products = _productRepository.GetAllProducts().ToList();
            
            _service = new CustomerService("customer1",
                _customerRepository, _productRepository);
        }

        [Test]
        [TestCase(3)]
        public void AddToOrder_ShouldAddProductToOrderList(int quantity)
        {
            var customer = new Customer
            {
                ToOrder = new List<ProductOrder>
                {
                    new ProductOrder {Product = _products[0], Quantity = quantity}
                }
            };

            _service.AddToOrder(_products[0], quantity);
            
            Assert.AreEqual(customer.ToOrder[0].Product, _service.Customer.ToOrder[0].Product);
        }

        [Test]
        [TestCase(3)]
        public void AddToOrder_ShouldAddProductToCurrentOrder(int quantity)
        {
            var customer = new Customer
            {
                CurrentOrder = new Order
                {
                    Products = { new ProductOrder {Product = _products[0], Quantity = quantity}}
                }
            };
            
            _service.AddToOrder(_products[0], quantity);
            
            Assert.AreEqual(customer.CurrentOrder.TotalPrice, _service.Customer.CurrentOrder.TotalPrice);
        }

        [Test]
        public void CancelCurrentOrder_ShouldCreateEmptyToOrderList()
        {
            var customer = new Customer
            {
                ToOrder = new List<ProductOrder>()
            };
            
            _service.CancelCurrentOrder();
            
            Assert.AreEqual(customer.ToOrder, _service.Customer.ToOrder);
        }

        [Test]
        public void CreateOrder_ShouldAddOrderToPlacedOrders()
        {
            var order = new Order
            {
                Products = { new ProductOrder {Product = _products[0], Quantity = 3}}
            };
            var customer = new Customer {CurrentOrder = order};

            _service.CreateOrder();
            
            Assert.AreEqual(customer.PlacedOrders, _service.Customer.PlacedOrders);
        }

        [Test]
        public void CreateOrder_ShouldCreateEmptyOrderInstance()
        {
            var customer = new Customer();

            _service.CreateOrder();
            
            Assert.AreEqual(customer.CurrentOrder.TotalPrice, _service.Customer.CurrentOrder.TotalPrice);
        }
        
        [Test]
        public void CreateOrder_ShouldCreateEmptyToOrderList()
        {
            var customer = new Customer();

            _service.CreateOrder();
            
            Assert.AreEqual(customer.ToOrder.Capacity, _service.Customer.ToOrder.Capacity);
        }

        [Test]
        [TestCase("user", "slklasd@sd.com", "380986018821", "street 9")]
        [TestCase("user2", "email@ser.com", "380239023", "avenue 9")]
        public void ChangeInfo_ShouldChangeInfoOnCorrect(string name, string email, string number, string address)
        {
            var customer = new Customer
            {
                Name = name,
                Email = email,
                PhoneNumber = number,
                Address = address
            };

            _service.ChangeInfo(name, email, number, address);

            Assert.AreEqual(customer.Name, _service.Customer.Name);
            Assert.AreEqual(customer.Email, _service.Customer.Email);
            Assert.AreEqual(customer.PhoneNumber, _service.Customer.PhoneNumber);
            Assert.AreEqual(customer.Address, _service.Customer.Address);
        }

        [Test]
        public void ChangeStatus_ShouldChangeOnReceivedStatus()
        {
            // Arrange
            var order = new Order
            {
                Products = { new ProductOrder {Product = _products[0], Quantity = 3}}
            };
            var customer = new Customer
            {
                PlacedOrders =
                {
                    new Order {Status = OrderStatus.Received}
                }
            };
            
            _service.Customer.PlacedOrders = new List<Order>
            {
                new Order()
            };
            
            // Act
            _service.ChangeStatus(0);
            
            // Assert
            Assert.AreEqual(customer.CurrentOrder.Status, _service.Customer.CurrentOrder.Status);
        }

        public void GetPlacedOrders_ShouldReturnAllPlacedOrders()
        {
            _service.Customer.PlacedOrders = new List<Order>
            {
                new Order(),
                new Order()
            };

            var result = _service.GetPlacedOrders();

            Assert.AreEqual(_service.Customer.PlacedOrders.Count(), result.Count());
        }
    }
}