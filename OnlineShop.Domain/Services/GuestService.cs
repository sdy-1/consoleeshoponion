﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using OnlineShop.Data.Models;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services.Interfaces;

namespace OnlineShop.Domain.Services
{
    public class GuestService : ServiceBase
    {
        protected AdminRepository AdminRepository { get; }

        public GuestService(AdminRepository adminRepository, CustomerRepository customerRepository,
            ProductRepository productRepository)
            : base(customerRepository, productRepository)
        {
            AdminRepository = adminRepository;
        }

        public bool SignUp(string login, string password)
        {
            if (CustomerRepository.GetAllCustomers().Any(c => c.Login == login))
                return false;

            var customer = new Customer
            {
                Login = login,
                Password = password
            };

            CustomerRepository.Create(customer);

            return true;
        }

        public int SignIn(string login, string password)
        {
            var customer = CustomerRepository.GetAllCustomers()
                  .FirstOrDefault(c => c.Login == login);

            if (customer is { })
            {
                if (customer.Password == password)
                    return 0;
            }

            var admin = AdminRepository.GetAllAdmins()
                .FirstOrDefault(a => a.Login == login);
            
            if (admin is { })
            {
                if (admin.Password == password)
                    return 1;
            }

            return -1;
        }

        public bool SetPersonalInfo(string customerLogin, string name, string number,
            DateTime birthDate, string email, string address)
        {
            var customer = CustomerRepository.GetAllCustomers()
                .FirstOrDefault(c => c.Login == customerLogin);

            if (customer is null)
                return false;

            customer.Name = name;
            customer.PhoneNumber = number;
            customer.BirthDate = birthDate;
            customer.Email = email;
            customer.Address = address;

            return true;
        }
    }
}