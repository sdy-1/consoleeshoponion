﻿using OnlineShop.Data.Models;

namespace OnlineShop.Domain.Services.Interfaces
{
    public interface IChangeStatusOrder
    {
        void ChangeStatus(int orderNumber, OrderStatus orderStatus, string customerLogin);
    }
}