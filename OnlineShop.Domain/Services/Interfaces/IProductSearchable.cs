﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using OnlineShop.Data.Models;

namespace OnlineShop.Domain.Services.Interfaces
{
    public interface IProductSearchable
    {
        IEnumerable<Product> GetAllProduct();
        Product FindProduct(string name);
    }
}