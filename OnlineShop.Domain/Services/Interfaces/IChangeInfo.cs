﻿using OnlineShop.Data.Models;

namespace OnlineShop.Domain.Services.Interfaces
{
    public interface IChangeInfo
    {
        public void ChangeInfo(string name, string email, string number, string address, string customerLogin);
    }
}