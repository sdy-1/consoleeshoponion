﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using OnlineShop.Data.Models;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services.Interfaces;

namespace OnlineShop.Domain.Services
{
    public class CustomerService : ServiceBase, IChangeInfo, IChangeStatusOrder
    {
        public Customer Customer { get; }

        public CustomerService(string customerLogin, CustomerRepository customerRepository,
            ProductRepository productRepository)
            : base(customerRepository, productRepository)
        {
            Customer = customerRepository.GetAllCustomers().First(c => c.Login == customerLogin);
        }

        public void AddToOrder(Product product, int productQuantity)
        {
            var productOrder = new ProductOrder
            {
                Product = product,
                Quantity = productQuantity
            };
            
            Customer.ToOrder.Add(productOrder);
            Customer.CurrentOrder.Products.Add(productOrder);
        }

        public void CancelCurrentOrder() =>
            Customer.ToOrder = new List<ProductOrder>();
        
        public void CreateOrder()
        {
            if (!Customer.ToOrder.Any())
                return;
            
            Customer.PlacedOrders.Add(Customer.CurrentOrder);
            Customer.CurrentOrder = new Order();
            Customer.ToOrder = new List<ProductOrder>();
        }
        
        public void ChangeInfo(string name, string email, string number, string address, string customerLogin = null)
        {
            Customer.Name = name;
            Customer.Email = email;
            Customer.PhoneNumber = number;
            Customer.Address = address;
        }

        public void ChangeStatus(int orderNumber, OrderStatus orderStatus = OrderStatus.Received, string customerLogin = null)
        {
            var toChange = Customer.PlacedOrders.ElementAt(orderNumber);
            toChange.Status = orderStatus;
        }

        public IEnumerable<Order> GetPlacedOrders() => Customer.PlacedOrders;
    }
}