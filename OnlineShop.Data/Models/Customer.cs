﻿using System;
using System.Collections.Generic;

namespace OnlineShop.Data.Models
{
    public class Customer : RegisteredUsers
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }

        public List<ProductOrder> ToOrder { get; set; } = new List<ProductOrder>();
        public List<Order> PlacedOrders { get; set; } = new List<Order>();

        public Order CurrentOrder { get; set; } = new Order();

        public override string ToString() =>
            $"Name: {Name}\nbirth date: {BirthDate}\nemail: {Email}\n" +
            $"phone number: {PhoneNumber}\nAddress: {Address}";
    }
}