﻿namespace OnlineShop.Data.Models
{
    public enum Category
    {
        Fruit,
        Drink,
        Vegetable,
        Meat
    }
    
    public class Product
    {
        public string Name { get; set; }
        
        public decimal Price { get; set; }
        
        public Category Category { get; set; }
        
        public string Description { get; set; }

        public Product(string name, decimal price, string description, Category category)
        {
            Name = name;
            Price = price;
            Description = description;
            Category = category;
        }

        public override string ToString() =>
            $"Name: {Name}, Category: {Category}, Price: {Price}";
    }
}