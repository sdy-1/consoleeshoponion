﻿namespace OnlineShop.Data.Models
{
    public class ProductOrder
    {
        public Product Product { get; set; }
        
        public int Quantity { get; set; }

        public decimal TotalPrice => Product.Price * Quantity;

        public override string ToString() =>
            $"{Product}, Total: {TotalPrice}";
    }
}