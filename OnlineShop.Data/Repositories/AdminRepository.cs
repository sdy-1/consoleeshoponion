﻿using System.Collections.Generic;
using OnlineShop.Data.Models;

namespace OnlineShop.Data.Repositories
{
    public class AdminRepository
    {
        private List<Admin> _admins = new List<Admin>();

        public AdminRepository()
        {
            _admins.Add(new Admin { Login = "admin", Password = "admin" });
        }

        public IEnumerable<Admin> GetAllAdmins() => _admins;
    }
}