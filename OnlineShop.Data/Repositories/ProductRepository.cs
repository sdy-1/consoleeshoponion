﻿using System.Collections.Generic;
using System.Linq;
using OnlineShop.Data.Models;

namespace OnlineShop.Data.Repositories
{
    public class ProductRepository
    {
        private List<Product> _products = new List<Product>();

        public ProductRepository()
        {
            var products = new []
            {
                new Product("Apple", 10, "Fruit", Category.Fruit),
                new Product("Juice", 12, "Some tasty thing", Category.Drink),
                new Product("Carrot", 4, "Vegetable", Category.Vegetable),
                new Product("Chicken", 30, "From country", Category.Meat),
                new Product("Fish", 43, "With love from Black Sea", Category.Meat)
            };
            
            _products.AddRange(products);
        }
        
        public IEnumerable<Product> GetAllProducts() => _products;

        public void Create(Product product) => 
            _products.Add(product);
    }
}