﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text.RegularExpressions;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;
using OnlineShop.Domain.Services;

namespace OnlineShop.ConsoleUI.Commands
{
    public class ChangeMyInfoCommand : CommandBase
    {
        public override string Name => "change info";
        public override string Description => "Changes personal information";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var customerController = (CustomerController) controllerBase;
            var service = customerController.CustomerService;

            Console.WriteLine("Current information: ");
            Console.WriteLine(service.Customer);
            
            Console.WriteLine("Press \"Y\" to change all personal information or \"N\" to leave");
            ConsoleKeyInfo key;
            while (true)
            {
                key = Console.ReadKey();
                Console.WriteLine();
                switch (key.Key)
                {
                    case ConsoleKey.Y:
                        service.ChangeInfo(ChangeName(), ChangeEmail(),
                            ChangePhoneNumber(), ChangeAddress());
                        Console.WriteLine(service.Customer);
                        break;

                    case ConsoleKey.N:
                        break;

                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("You have pressed a wrong key");
                        Console.ResetColor();
                        continue;
                }
                break;
            }

            return customerController;
        }
        
        private static string ChangeName()
        {
            while (true)
            {
                Console.WriteLine("Enter a first and second name");
                var name = Console.ReadLine();

                var pattern = @"[a-zA-Z]";
                if (name is { } && Regex.IsMatch(name, pattern))
                    return name;
                
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }
        
        private static string ChangeEmail()
        {
            while (true)
            {
                Console.WriteLine("Enter an email");
                var email = Console.ReadLine();

                try
                {
                    if (email is { })
                        _ = new MailAddress(email);
                }
                catch (FormatException)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Incorrect email format");
                    Console.ResetColor();
                    continue;
                }

                return email;
            }
        }
        
        private static string ChangeAddress()
        {
            while (true)
            {
                Console.WriteLine("Enter an address");
                var address = Console.ReadLine();

                if (address is { })
                    return address;

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Incorrect address");
                Console.ResetColor();
            }
        }
        
        private static string ChangePhoneNumber()
        {
            while (true)
            {
                Console.WriteLine("Enter a phone number");
                var number = Console.ReadLine();

                var pattern = @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}";
                if (number is { } && Regex.IsMatch(number, pattern))
                    return number;
                
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Incorrect phone");
                Console.ResetColor();
            }
        }
    }
}