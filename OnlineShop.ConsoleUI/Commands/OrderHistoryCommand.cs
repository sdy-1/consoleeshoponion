﻿using System;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class OrderHistoryCommand : CommandBase
    {
        public override string Name => "history";
        public override string Description => "Shows history of your orders";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var customerController = (CustomerController) controllerBase;
            var service = customerController.CustomerService;

            var orders = service.GetPlacedOrders();

            int number = 1;
            foreach (var order in orders)
            {
                Console.Write($"{number++}. ");
                foreach (var product in order.Products)
                {
                    Console.WriteLine($"{product}");
                }

                Console.WriteLine($"Total price: {order.TotalPrice}\n");
            }

            return customerController;
        }
    }
}