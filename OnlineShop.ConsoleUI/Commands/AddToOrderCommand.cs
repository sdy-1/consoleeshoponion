﻿using System;
using System.Transactions;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class AddToOrderCommand : CommandBase
    {
        public override string Name => "add to order";
        public override string Description => "Add new item to order";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var customerController = (CustomerController) controllerBase;
            var service = customerController.CustomerService;

            Console.WriteLine("Enter a product name or \"0\" to exit");
            while (true)
            {
                var name = Console.ReadLine();

                switch (name)
                {
                    case "0":
                        return controllerBase;

                    default:
                        var product = service.FindProduct(name);
                        if (product is null)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("There is no such product in a shop");
                            Console.ResetColor();
                            continue;
                        }

                        Console.WriteLine("Enter a quantity of selected product");
                        int quantity;
                        while (true)
                        {
                            if (int.TryParse(Console.ReadLine(), out quantity))
                                break;
                            
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Wrong number");
                            Console.ResetColor();
                        }

                        service.AddToOrder(product, quantity);
                        Console.WriteLine($"You have successfully added {quantity} {product.Name}(s) to your order");
                        break;
                }

                break;
            }

            return customerController;
        }
    }
}