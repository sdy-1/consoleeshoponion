﻿using System;
using System.Linq;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class CancelCurrentOrderCommand : CommandBase
    {
        public override string Name => "cancel";
        public override string Description => "Cancel current order";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var customerController = (CustomerController) controllerBase;
            var service = customerController.CustomerService;
            
            Console.WriteLine("Your current order:");
            foreach (var productOrder in service.Customer.ToOrder)
            {
                Console.WriteLine($"{productOrder}");
            }
            Console.WriteLine("Total price: " + service.Customer.ToOrder.Sum(o => o.TotalPrice));
            
            Console.WriteLine("Press \"Y\" to cancel or \"N\" to leave");
            ConsoleKeyInfo key;
            while (true)
            {
                key = Console.ReadKey();
                Console.WriteLine();
                switch (key.Key)
                {
                    case ConsoleKey.Y:
                        service.CancelCurrentOrder();
                        Console.WriteLine("You have cancel your order, shopping cart is empty");
                        break;

                    case ConsoleKey.N:
                        return customerController;

                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("You have pressed a wrong key");
                        Console.ResetColor();
                        break;
                }

                break;
            }

            return customerController;
        }
    }
}