﻿using System;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class LogOutCommand : CommandBase
    {
        public override string Name => "log out";
        public override string Description => "Logs out from current account";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            Console.WriteLine("You have successfully logged out");
            return new GuestController(controllerBase.AdminRepository,
                controllerBase.CustomerRepository,
                controllerBase.ProductRepository);
        }
    }
}