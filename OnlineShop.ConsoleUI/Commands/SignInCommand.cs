﻿using System;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class SignInCommand : CommandBase
    {
        public override string Name => "sign in";
        public override string Description => "Sign in as an existing user";

        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var guestController = (GuestController) controllerBase;
            var service = guestController.GuestService;

            int result;
            string login;
            while (true)
            {
                Console.WriteLine("Enter login");
                login = Console.ReadLine();
                Console.WriteLine("Enter password");
                var pass = Console.ReadLine();

                result = service.SignIn(login, pass);
                if (result != -1) break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You have entered a wrong login or password");
                Console.ResetColor();
            }

            if (result == 0)
            {
                var customerController = new CustomerController(guestController.AdminRepository,
                    guestController.CustomerRepository, guestController.ProductRepository, login);
                
                new HelpCommand().Execute(customerController);
                
                Console.WriteLine("You have successfully signed in");
                return customerController;
            }

            var adminController = new AdminController(guestController.AdminRepository,
                guestController.CustomerRepository, guestController.ProductRepository);
            
            Console.Clear();
            new HelpCommand().Execute(adminController);
            
            Console.WriteLine("You have successfully signed in");
            return adminController;
        }
    }
}