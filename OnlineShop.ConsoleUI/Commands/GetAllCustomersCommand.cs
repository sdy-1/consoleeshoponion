﻿using System;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class GetAllCustomersCommand : CommandBase
    {
        public override string Name => "get customers";
        public override string Description => "Shows all registered customers";
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var adminController = (AdminController) controllerBase;
            var service = adminController.AdminService;

            var customers = service.GetAllCustomers();
            foreach (var customer in customers)
            {
                Console.WriteLine(customer + "\n");
            }

            return controllerBase;
        }
    }
}