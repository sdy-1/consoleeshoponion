﻿using System;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class HelpCommand : CommandBase
    {
        public override string Name => "help";
        public override string Description => "Show all available commands";

        public override ControllerBase Execute(ControllerBase controller)
        {
            var commands = controller.Commands;

            Console.Clear();
            
            Console.WriteLine("Available commands");
            foreach (var command in commands)
            {
                Console.WriteLine($"{command.Name} \t - \t {command.Description}");
            }

            return controller;
        }
    }
}