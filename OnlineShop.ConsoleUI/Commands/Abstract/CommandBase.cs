﻿using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands.Abstract
{
    public abstract class CommandBase
    {
        public abstract string Name { get; }
        public abstract string Description { get; }

        public abstract ControllerBase Execute(ControllerBase controllerBase);
    }
}