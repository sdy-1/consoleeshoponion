﻿using System;
using System.Linq;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;
using OnlineShop.Data.Models;

namespace OnlineShop.ConsoleUI.Commands
{
    public class ChangeOrderStatusCommand : CommandBase
    {
        public override string Name => "change status";
        public override string Description => "Changes order status of selected order of selected customer";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var adminController = (AdminController) controllerBase;
            var service = adminController.AdminService;

            Console.WriteLine("Enter a login of customer or \"0\" to leave");

            Customer customer;
            string login;
            while (true)
            {
                login = Console.ReadLine();
                Console.WriteLine();
                switch (login)
                {
                    case "0":
                        return adminController;

                    default:
                        customer = service.FindCustomer(login);
                        if (customer is { })
                            break;
                        
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("There is no customer with such login");
                        Console.ResetColor();
                        continue;
                }

                break;
            }

            var orders = customer.PlacedOrders;
            
            int number = 1;
            foreach (var order in orders)
            {
                Console.Write($"{number++}. ");
                foreach (var product in order.Products)
                {
                    Console.WriteLine($"{product}");
                }

                Console.WriteLine($"Total price: {order.TotalPrice}");
                Console.WriteLine($"Status: {order.Status}\n");
            }

            Console.WriteLine("Choose an order which status you want to change");
            int choose;
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out choose)
                    && choose >= 1 && choose <= number - 1)
                    break;
                
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Choose the correct order number");
                Console.ResetColor();
            }

            OrderStatus status;
            while (true)
            {
                Console.WriteLine("Enter an order status");

                if (Enum.TryParse(Console.ReadLine(), out status))
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect status of order");
                Console.ResetColor();
            }
            
            service.ChangeStatus(choose - 1, status, login);
            Console.WriteLine(orders.ElementAt(choose - 1));

            return adminController;
        }
    }
}