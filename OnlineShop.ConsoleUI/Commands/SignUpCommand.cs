﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;
using OnlineShop.Domain.Services;

namespace OnlineShop.ConsoleUI.Commands
{
    public class SignUpCommand : CommandBase
    {
        public override string Name => "sign up";
        public override string Description => "Sign up a new user";

        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var guestController = (GuestController) controllerBase;

            var service = guestController.GuestService;

            var login = SetSecurityInfo(service);
            var name = SetName();
            var number = SetNumber();
            var birthDate = SetBirthDate();
            var email = SetEmail();
            var address = SetAddress();

            service.SetPersonalInfo(login, name, number, birthDate, email, address);
            Console.WriteLine("You have successfully signed up");
            
            var customerController = new CustomerController(guestController.AdminRepository, 
                guestController.CustomerRepository, guestController.ProductRepository, login);

            Console.Clear();
            new HelpCommand().Execute(customerController);
            
            return customerController;
        }

        private static string SetAddress()
        {
            string address;
            while (true)
            {
                Console.WriteLine("Enter your address");
                address = Console.ReadLine();

                if (address is { })
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }

            return address;
        }

        private static string SetEmail()
        {
            string email;
            while (true)
            {
                Console.WriteLine("Enter an email");
                email = Console.ReadLine();

                try
                {
                    if (email is { })
                        _ = new MailAddress(email);
                }
                catch (FormatException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Incorrect format of email");
                    Console.ResetColor();
                    continue;
                }

                break;
            }

            return email;
        }

        private static DateTime SetBirthDate()
        {
            DateTime birthDate;
            while (true)
            {
                Console.WriteLine("Enter a date of birth");

                if (DateTime.TryParse(Console.ReadLine(), out birthDate)
                    && DateTime.Now.Year - birthDate.Year < 105 && DateTime.Now.Year - birthDate.Year > 6)
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect date of birth");
                Console.ResetColor();
            }

            return birthDate;
        }

        private static string SetNumber()
        {
            string number;
            while (true)
            {
                Console.WriteLine("Enter a phone number");
                number = Console.ReadLine();

                var pattern = @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}";
                if (number is { } && Regex.IsMatch(number, pattern))
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect phone number");
                Console.ResetColor();
            }

            return number;
        }

        private static string SetName()
        {
            string name;
            while (true)
            {
                Console.WriteLine("Enter a first and second name");
                name = Console.ReadLine();

                var pattern = @"[a-zA-Z]";
                if (name is { } && Regex.IsMatch(name, pattern))
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }

            return name;
        }

        private static string SetSecurityInfo(GuestService service)
        {
            string login;
            while (true)
            {
                string password;
                while (true)
                {
                    Console.WriteLine("Enter a login");
                    login = Console.ReadLine();

                    Console.WriteLine("Enter a password");
                    password = Console.ReadLine();

                    Console.WriteLine("Enter your password again");
                    var checkPassword = Console.ReadLine();

                    if (password == checkPassword) break;

                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Password does not match");
                    Console.ResetColor();
                }

                if (service.SignUp(login, password))
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Customer with such login is already registered");
                Console.ResetColor();
            }
            
            Console.Clear();

            return login;
        }
    }
}