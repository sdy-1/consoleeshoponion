﻿using System;
using System.Linq;
using OnlineShop.ConsoleUI.Commands;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;
using OnlineShop.Data.Repositories;

namespace OnlineShop.ConsoleUI
{
    public class Program
    {
        static void Main(string[] args)
        {
            var adminRepo = new AdminRepository();
            var customerRepo = new CustomerRepository();
            var productRepo = new ProductRepository();
            
            ControllerBase controller = new GuestController(adminRepo,customerRepo, productRepo);
            new HelpCommand().Execute(controller);

            while (true)
            {
                Console.Write("> ");
                var line = Console.ReadLine();
                if (line == "exit")
                    break;
                var command = controller.Commands.FirstOrDefault(x => x.Name == line);

                if (command is null)
                {
                    Console.WriteLine("There is no such command, type help.");
                    continue;
                }

                controller = command.Execute(controller);
            }
        }
    }
}